﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISBN_Numbers
{
    class ProgramISBN
    {
        public class ISBNten
        {
            private string isbnNum;

            public string GetISBN10()
            {
                return this.isbnNum;

            }
            public ISBNten()
            {  
                Console.Write("Input the ISBN-10:   ");
                this.isbnNum = Console.ReadLine();
            }

            public string displayIsbn()
            {
                
                return this.GetISBN10();
            }
        }
        public static void Main(string[] args)
        {
            // creates a new instance of ISBNten
            ISBNten Book = new ISBNten();
            
            //Calls to the ValidateIsbn to get the return value 
            bool Result = Authentication.ValidateIsbn(Book.GetISBN10());
            
            // boolean ouptut of valid isbn for true, not a valid isbn for false
            if (Result == true)
                Console.WriteLine("This is a valid ISBN-10");


            else Console.WriteLine("This is not a valid ISBN-10");

            Console.ReadLine();
            

        }
    }
}
