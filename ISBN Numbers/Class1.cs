﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISBN_Numbers
{
    public class Authentication
    {
        public static string RewriteIsbn(string isbn)
        {
            // removes the dash mark in the isbn input
            return isbn.Replace("-", "").Replace(" ", "");
        }
        public static bool ValidateIsbn(string isbn)
        {
            // Checks if the number input is 10 numbers in length
            isbn = RewriteIsbn(isbn);
            if (isbn.Length != 10)
                return false;
            
            // runs the try parse without the need of creating a new variable 
            int outcome;
            for (int i = 0; i < 9; i++)
                if (!int.TryParse(isbn[i].ToString(), out outcome))
                    return false;
            
            //sums the isbn number inputs outside of the tenth digit
            int sum = 0;
            for (int i = 0; i < 9; i++)
                sum += (i + 1) * int.Parse(isbn[i].ToString());
            
            int remainder = sum % 11;
            if (remainder == 10)
                return isbn[9] == 'X';
            else
                return isbn[9] == (char)('0' + remainder);
        }


    }
}